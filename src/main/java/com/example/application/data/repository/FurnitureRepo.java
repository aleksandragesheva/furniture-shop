/*
 * FurnitureRepo.java
 *
 * created at Jan 11, 2023 by aleksandra aleksandra@seeburger.com
 *
 * Copyright © SEEBURGER AG, Germany. All Rights Reserved.
 */

package com.example.application.data.repository;

import com.example.application.data.entity.Furniture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;

public interface FurnitureRepo
        extends JpaRepository<Furniture, Long>, JpaSpecificationExecutor<Furniture> {
}
