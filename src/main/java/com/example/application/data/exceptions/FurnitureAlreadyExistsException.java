/*
 * FurnitureWithTypeAndColorAlreadyExistsException.java
 *
 * created at Nov 11, 2022 by a.gesheva a.gesheva@seeburger.com
 *
 * Copyright © SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.example.application.data.exceptions;


public class FurnitureAlreadyExistsException
                extends RuntimeException
{

    public FurnitureAlreadyExistsException(String message)
    {
        super(message);
    }

}
