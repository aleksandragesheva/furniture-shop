/*
 * CartItemServiceImpl.java
 *
 * created at Nov 02, 2022 by a.gesheva a.gesheva@seeburger.com
 *
 * Copyright © SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.example.application.data.service;


import com.example.application.data.bean.CartItemBean;
import com.example.application.data.entity.CartItem;
import com.example.application.data.entity.Furniture;
import com.example.application.mapper.CartItemMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CartItemService
{
    private final JpaRepository<CartItem, Long> repository;

    public CartItemService(JpaRepository<CartItem, Long> repository) {
        this.repository = repository;
    }

    public Optional<CartItem> get(Long id)
    {
        return repository.findById(id);
    }

    public CartItem create(Furniture furniture)
    {
        CartItem cartItem = new CartItem();
        cartItem.setFurniture(furniture);
        cartItem.setCount(1);
        return repository.save(cartItem);
    }


    public CartItem update(CartItem entity)
    {
        return repository.save(entity);
    }


    public void delete(Long id)
    {
        repository.deleteById(id);
    }


    public Page<CartItemBean> list(Pageable pageable)
    {
        CartItemMapper mapper = new CartItemMapper();
        Page<CartItem> cartItem = repository.findAll(pageable);
        Page<CartItemBean> cartItemBeans = new PageImpl<>(mapper.cartItemEntitiesToCartItemBeans(cartItem));
        return cartItemBeans;
    }


    public int count()
    {
        return (int)repository.count();
    }

    JpaRepository<CartItem, Long> getRepo(){
        return repository;
    }

}
