package com.example.application.data.service;

import com.example.application.data.entity.Shop;
import com.example.application.data.repository.ShopRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ShopService {

    private final ShopRepo repository;

    public ShopService(ShopRepo repository) {
        this.repository = repository;
    }



    public Optional<Shop> get(Long id)
    {
        return repository.findById(id);
    }


    public Shop update(Shop entity)
    {
        return repository.save(entity);
    }


    public void delete(Long id)
    {
        repository.deleteById(id);
    }



    public Page<Shop> list(Pageable pageable)
    {
        return repository.findAll(pageable);
    }

    public Page<Shop> list(Pageable pageable, Specification<Shop> filter) {
        return repository.findAll(filter, pageable);
    }


    public int count()
    {
        return (int)repository.count();
    }

    JpaRepository<Shop, Long> getRepo(){
        return repository;
    }
}
