/*
 * FurnitureServiceImpl.java
 *
 * created at Oct 30, 2022 by a.gesheva a.gesheva@seeburger.com
 *
 * Copyright © SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.example.application.data.service;


import com.example.application.data.entity.Furniture;
import com.example.application.data.repository.FurnitureRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FurnitureService
{

    private final FurnitureRepo repository;

    public FurnitureService(FurnitureRepo repository) {
        this.repository = repository;
    }



    public Optional<Furniture> get(Long id)
    {
        return repository.findById(id);
    }


    public Furniture update(Furniture entity)
    {
        return repository.save(entity);
    }


    public void delete(Long id)
    {
        repository.deleteById(id);
    }



    public Page<Furniture> list(Pageable pageable)
    {
        return repository.findAll(pageable);
    }

    public Page<Furniture> list(Pageable pageable, Specification<Furniture> filter) {
        return repository.findAll(filter, pageable);
    }


    public int count()
    {
        return (int)repository.count();
    }

    JpaRepository<Furniture, Long> getRepo(){
        return repository;
    }
}
