/*
 * FurnitureEntity.java
 *
 * created at Oct 20, 2022 by a.gesheva a.gesheva@seeburger.com
 *
 * Copyright © SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.example.application.data.entity;


import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * Entity which contains the necessary information for tFurniture table.
 */
@Entity
@Table(name = "furniture")
public class Furniture extends AbstractEntity
{
    @Column
    private String type;

    @Column
    private String color;

    @Column
    private boolean available;

    @Column
    private BigDecimal price;

    @OneToMany(mappedBy = "furniture",
            fetch=FetchType.EAGER,
               cascade = CascadeType.ALL)
    @BatchSize(size = 1000)
    private List<CartItem> cartItems;


    public Furniture()
    {
    }


    public String getType()
    {
        return type;
    }


    public void setType(String type)
    {
        this.type = type;
    }


    public String getColor()
    {
        return color;
    }


    public void setColor(String color)
    {
        this.color = color;
    }


    public boolean isAvailable()
    {
        return available;
    }


    public void setAvailable(boolean available)
    {
        this.available = available;
    }


    public BigDecimal getPrice()
    {
        return price;
    }


    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }


    public List<CartItem> getCartItems()
    {
        return cartItems;
    }


    public void setCartItems(List<CartItem> cartItems)
    {
        this.cartItems = cartItems;
    }


}
