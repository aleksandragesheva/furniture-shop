/*
 * CartItemEntity.java
 *
 * created at Nov 02, 2022 by a.gesheva a.gesheva@seeburger.com
 *
 * Copyright © SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.example.application.data.entity;


import javax.persistence.*;


/**
 * Entity which contains the necessary information for tCartItem table.
 */
@Entity
@Table(name = "cart_item")
public class CartItem extends AbstractEntity
{

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "cart_item_furniture_id", referencedColumnName = "id")
    private Furniture furniture;

    @Column
    private int count;



    public Furniture getFurniture()
    {
        return furniture;
    }


    public void setFurniture(Furniture furniture)
    {
        this.furniture = furniture;
    }


    public int getCount()
    {
        return count;
    }


    public void setCount(int count)
    {
        this.count = count;
    }
}
