package com.example.application.views.мебели;

import com.example.application.data.entity.Furniture;
import com.example.application.data.service.CartItemService;
import com.example.application.data.service.FurnitureService;
import com.example.application.views.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.data.VaadinSpringDataHelpers;
import com.vaadin.flow.theme.lumo.LumoUtility;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;


/**
 * This class represents a sub screen which is opened from the {@link FurnitureGridScreen} when the logged user wants to add or update a
 * product and holds inside the {@link FurnitureForm}
 */
@PageTitle("Мебели")
@Route(value = "furniture", layout = MainLayout.class)
@Uses(Icon.class)
public class FurnitureScreen extends Div
{

    private Grid<Furniture> grid;
    private Button addButton;
    private Button editButton;
    private Button removeButton;
    private Button addToCart;
    private Filters filters;
    private final FurnitureService furnitureService;
    private final CartItemService cartItemService;

    public FurnitureScreen(FurnitureService furnitureService, CartItemService cartItemService) {
        this.furnitureService = furnitureService;
        this.cartItemService = cartItemService;
        setSizeFull();
        addClassNames("furniture-view");

        filters = new Filters(() -> refreshGrid());
        VerticalLayout layout = new VerticalLayout(createMobileFilters(), filters, addOwnerButtons(), createGrid());
        layout.setSizeFull();
        layout.setPadding(false);
        layout.setSpacing(false);
        add(layout);
    }

    private HorizontalLayout createMobileFilters() {
        // Mobile version
        HorizontalLayout mobileFilters = new HorizontalLayout();
        mobileFilters.setWidthFull();
        mobileFilters.addClassNames(LumoUtility.Padding.MEDIUM, LumoUtility.BoxSizing.BORDER,
                LumoUtility.AlignItems.CENTER);
        mobileFilters.addClassName("mobile-filters");

        Icon mobileIcon = new Icon("lumo", "plus");
        Span filtersHeading = new Span("Filters");
        mobileFilters.add(mobileIcon, filtersHeading);
        mobileFilters.setFlexGrow(1, filtersHeading);
        mobileFilters.addClickListener(e -> {
            if (filters.getClassNames().contains("visible")) {
                filters.removeClassName("visible");
                mobileIcon.getElement().setAttribute("icon", "lumo:plus");
            } else {
                filters.addClassName("visible");
                mobileIcon.getElement().setAttribute("icon", "lumo:minus");
            }
        });
        return mobileFilters;
    }

    public static class Filters extends Div implements Specification<Furniture> {

        private final TextField type = new TextField("Type");
        private final TextField color = new TextField("Color");

        public Filters(Runnable onSearch) {

            setWidthFull();
            addClassName("filter-layout");
            addClassNames(LumoUtility.Padding.Horizontal.LARGE, LumoUtility.Padding.Vertical.MEDIUM,
                    LumoUtility.BoxSizing.BORDER);
            type.setPlaceholder("Type");


            // Action buttons
            Button resetBtn = new Button("Reset");
            resetBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
            resetBtn.addClickListener(e -> {
                type.clear();
                color.clear();
                onSearch.run();
            });
            Button searchBtn = new Button("Search");
            searchBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
            searchBtn.addClickListener(e -> onSearch.run());

            Div actions = new Div(resetBtn, searchBtn);
            actions.addClassName(LumoUtility.Gap.SMALL);
            actions.addClassName("actions");

            add(type, color, actions);
        }

        @Override
        public Predicate toPredicate(Root<Furniture> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
            List<Predicate> predicates = new ArrayList<>();

            if (!type.isEmpty()) {
                String lowerCaseFilter = type.getValue().toLowerCase();
                Predicate typeMatch = criteriaBuilder.like(criteriaBuilder.lower(root.get("type")),
                        lowerCaseFilter + "%");
                predicates.add(typeMatch);
            }
            if (!color.isEmpty()) {
                String databaseColumn = "color";

                String lowerCaseFilter = color.getValue().toLowerCase();
                Predicate phoneMatch = criteriaBuilder.like(criteriaBuilder.lower(root.get(databaseColumn)),
                        "%" + lowerCaseFilter + "%");
                predicates.add(phoneMatch);

            }

            return criteriaBuilder.and(predicates.toArray(Predicate[]::new));
        }

    }

    private Component createGrid() {
        grid = new Grid<>(Furniture.class, false);
        grid.addColumn("type").setAutoWidth(true);
        grid.addColumn("color").setAutoWidth(true);
        grid.addColumn("available").setAutoWidth(true);
        grid.addColumn("price").setAutoWidth(true);

        grid.asSingleSelect().addValueChangeListener(l -> {
            final Furniture value = l.getValue();
            editButton.setEnabled(value != null);
            removeButton.setEnabled(value != null);
            addToCart.setEnabled(value != null);
        });

        grid.setItems(query -> furnitureService.list(
                PageRequest.of(query.getPage(), query.getPageSize(), VaadinSpringDataHelpers.toSpringDataSort(query)),
                filters).stream());
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.addClassNames(LumoUtility.Border.TOP, LumoUtility.BorderColor.CONTRAST_10);

        return grid;
    }

    private void refreshGrid() {
        grid.getDataProvider().refreshAll();
    }

    private Component addOwnerButtons()
    {
        addButton = new Button("Add", l -> {
            final Furniture furniture = new Furniture();
            openOwnerForm(furniture);
        });
        addButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    editButton =
        new Button(
            "Change",
            l -> {
              final Furniture furniture = grid.asSingleSelect().getValue();
              openOwnerForm(furniture);
            });
        editButton.setEnabled(false);
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        removeButton = new Button("Delete", l -> {
            final Dialog dialog = new Dialog();
            final Button yes = new Button("Yes", removeL -> {
                furnitureService.delete(grid.asSingleSelect().getValue().getId());
                dialog.close();
                refreshGrid();
            });
            final Button no = new Button("No", removeL -> dialog.close());
            final HorizontalLayout removeDialogButtons = new HorizontalLayout(yes, no);
            dialog.setWidth("350px");
            dialog.setHeight("200px");
            dialog.add(new VerticalLayout(new H3("Are you sure you want to delete it?"), removeDialogButtons));
            dialog.open();
        });
        removeButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        removeButton.setEnabled(false);
        addToCart = new Button("Add To Cart", l-> {
            cartItemService.create(grid.asSingleSelect().getValue());
        });
        return new HorizontalLayout(addButton, editButton, removeButton, addToCart);
    }


    private void openOwnerForm(Furniture furniture)
    {
        final Dialog dialog = new Dialog();
        final FurnitureForm furnitureForm = new FurnitureForm(furniture);
        furnitureForm.addCancelClickListener(closeL -> dialog.close());
        furnitureForm.addSaveClickListener(saveL -> {
            if (furnitureForm.isSaved())
            {
                furnitureService.update(furniture);
                dialog.close();
                refreshGrid();
            }
        });
        dialog.add(furnitureForm);
        dialog.setHeight("450px");
        dialog.setWidth("300px");
        dialog.setCloseOnEsc(true);
        dialog.setCloseOnOutsideClick(true);
        dialog.open();
    }
}
