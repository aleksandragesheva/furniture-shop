package com.example.application.views.мебели;


import com.example.application.data.entity.Furniture;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.ValidationException;


public class FurnitureForm extends VerticalLayout
{
    private TextField type = new TextField("Type");
    private TextField color = new TextField("Color");
    private TextField isAvailable = new TextField("Is Available");
    private TextField price = new TextField("Price");
    private boolean saved;

    private Furniture furniture;
    private BeanValidationBinder<Furniture> binder = new BeanValidationBinder<>(Furniture.class);
    private Button cancel;
    private Button save;


    public FurnitureForm(Furniture furniture)
    {
        this.furniture = furniture;
        init();
    }


    private void init()
    {
        setSizeFull();
        final FormLayout form = new FormLayout();
        form.add(type);
        form.add(color);
        form.add(isAvailable);
        form.add(price);
        binder.bindInstanceFields(this);
        binder.readBean(this.furniture);
        HorizontalLayout buttons = configureButtons();
        add(form, buttons);
    }


    private HorizontalLayout configureButtons()
    {
        save = new Button("Съхрани", l -> {
            if (binder.isValid())
            {
                try
                {
                    binder.writeBean(furniture);
                    saved = true;
                }
                catch (ValidationException e)
                {
                    e.printStackTrace();
                }
            }
        });
        cancel = new Button("Откажи");
        final HorizontalLayout buttons = new HorizontalLayout(save, cancel);
        return buttons;
    }


    public void addCancelClickListener(ComponentEventListener<ClickEvent<Button>> clickListener)
    {
        cancel.addClickListener(clickListener);
    }


    public void addSaveClickListener(ComponentEventListener<ClickEvent<Button>> clickListener)
    {
        save.addClickListener(clickListener);
    }


    public boolean isSaved()
    {
        return saved;
    }
}
