package com.example.application.views.cart;

import com.example.application.data.bean.CartItemBean;
import com.example.application.data.service.CartItemService;
import com.example.application.views.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.data.VaadinSpringDataHelpers;
import com.vaadin.flow.theme.lumo.LumoUtility;
import org.springframework.data.domain.PageRequest;

@PageTitle("Cart")
@Route(value = "Cart", layout = MainLayout.class)
@Uses(Icon.class)
public class CartView extends Div {

    private Grid<CartItemBean> grid;

    private final CartItemService cartItemService;

    public CartView(CartItemService cartItemService) {
        this.cartItemService = cartItemService;
        setSizeFull();
        addClassNames("cart-view");

        VerticalLayout layout = new VerticalLayout(createGrid());
        layout.setSizeFull();
        layout.setPadding(false);
        layout.setSpacing(false);
        add(layout);
    }

    private Component createGrid() {
        grid = new Grid<>(CartItemBean.class, false);
        grid.addColumn("type").setAutoWidth(true);
        grid.addColumn("price").setAutoWidth(true);
        grid.addColumn("count").setAutoWidth(true);

        grid.asSingleSelect().addValueChangeListener(l -> {
            final CartItemBean value = l.getValue();
        });

        grid.setItems(query -> cartItemService.list(
                PageRequest.of(query.getPage(), query.getPageSize(), VaadinSpringDataHelpers.toSpringDataSort(query))).stream());
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.addClassNames(LumoUtility.Border.TOP, LumoUtility.BorderColor.CONTRAST_10);

        return grid;
    }

    private void refreshGrid() {
        grid.getDataProvider().refreshAll();
    }

}
