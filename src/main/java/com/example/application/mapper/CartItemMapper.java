/*
 * ProductInformationMapper.java
 *
 * created at Nov 08, 2022 by a.gesheva a.gesheva@seeburger.com
 *
 * Copyright © SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.example.application.mapper;

import com.example.application.data.bean.CartItemBean;
import com.example.application.data.entity.CartItem;

import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public class CartItemMapper {

  public List<CartItemBean> cartItemEntitiesToCartItemBeans(Page<CartItem> cartItems) {
    List<CartItemBean> cartItemBeans = new ArrayList<>();
    for (CartItem cartItem : cartItems) {
      CartItemBean cartItemBean = new CartItemBean();
      cartItemBean.setId(cartItem.getId());
      cartItemBean.setType(cartItem.getFurniture().getType());
      cartItemBean.setPrice(cartItem.getFurniture().getPrice());
      cartItemBean.setCount(cartItem.getCount());
      cartItemBeans.add(cartItemBean);
    }
    return cartItemBeans;
  }

}
